from django.test import TestCase
from basic_test_app.models import Animal

"""
    You can show more detail at test file in basic_test_app
"""

# Create your tests here.
class AnimalTestCase(TestCase):
    def setUp(self):
        Animal.objects.create(name='lion', sound='roar')
        Animal.objects.create(name='cat', sound='meow')

    def test_aninals_can_speak(self):
        """Animals that can speak are correctly identified"""
        lion = Animal.objects.get(name='lion')
        cat = Animal.objects.get(name='cat')
        self.assertEqual(lion.speak(), 'The lion says "roar"')
        self.assertEqual(cat.speak(), 'The cat says "meow"')

