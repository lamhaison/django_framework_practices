from django.db import models
from modelchapter.inheritance_models import *

# Start for many to many relationship with Extra fields on many-to-many relationships
# Page 82 in http://media.readthedocs.org/pdf/django/1.8.x/django.pdf
# Create your models here.
class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    def __unicode__(self):  # __unicode__ on Python 2
        return self.first_name




class Group(models.Model):
    name = models.CharField(max_length=128)
    members = models.ManyToManyField(Person, through='Membership')

    def __str__(self):  # __unicode__ on Python 2
        return self.name


class Membership(models.Model):
    person = models.ForeignKey(Person)
    group = models.ForeignKey(Group)
    date_joined = models.DateField()
    invite_reason = models.CharField(max_length=64)

# End for many to many relationship with Extra fields on many-to-many relationships


# Start for Proxy Models
class MyPerson(Person):
    class Meta:
        ordering = ["last_name"]
        proxy = True

    def do_something(self):
        print '%s %s' % (self.first_name, self.last_name)