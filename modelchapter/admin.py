from django.contrib import admin
from modelchapter.models import Person, Membership, Group

# Register your models here.
admin.register(Person)
admin.register(Membership)
admin.register(Group)
