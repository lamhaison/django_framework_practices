__author__ = 'lhs'
from django.db import models

# Start for abstract base class ex
# Page 90
class CommonInfo(models.Model):
    name = models.CharField(max_length=100)
    age = models.PositiveIntegerField()

    class Meta:
        abstract = True
        ordering = ['name']


class Student(CommonInfo):
    address = models.CharField(max_length=100)

    class Meta(CommonInfo.Meta):
        db_table = 'student_info'

# End for abstract base class ex


# Start for Multi-table inheritance

class Place(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=80)

class Restaurant(Place):
    servers_hot_dogs = models.BooleanField(default=False)
    serves_pizza = models.BooleanField(default=False)

# End for Multi-table inheritance


