from django.db import models
from django.utils import timezone

# Create your models here.


class Options(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class Reporter(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField()

    def __unicode__(self):              # __unicode__ on Python 2
        return "%s %s" % (self.first_name, self.last_name)


class Article(models.Model):
    pub_date = models.DateField(help_text='yyyy-mm-dd', verbose_name='Published Date', default=timezone.now())
    headline = models.CharField(max_length=200)
    content = models.TextField()
    reporter = models.ForeignKey(Reporter)

    def __unicode__(self):         # __unicode__ on Python 2
        return self.headline

    class Meta:
        ordering = ('headline',)