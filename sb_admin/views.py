from django.shortcuts import render
from sb_admin.forms import TestForm, ArticleForm
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from sb_admin.models import *
from form_ex.forms import CommentForm, LoginForm
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.views import generic
from django.http import HttpResponseRedirect

import logging

logger = logging.getLogger(__name__)


# Create your views here.
def load_sb_dash_board(request):
    return render(request, 'sb-admin/pages/index.html')

def load_page(request, page_name):
    page = 'sb-admin/pages/%s.html' % page_name
    logger.debug(page)
    return render(request, page)


def load_my_page(request, page_name):

    page = 'sb-admin/pages/mypages/%s.html' % page_name
    logger.debug(page)
    if page_name == 'forms':
        return load_test_form_page(request, page_name)

    if page_name == 'bootstrapform':
        return load_test_form_page(request, page_name)

    if page_name == 'article_form':
        return load_articles_form_page(request, page_name)

    return render(request, page)


def load_test_form_page(request, page_name):

    if request.method == 'POST':
        form = TestForm(request.POST)
        logger.debug(form)
        logger.debug(form['char_field'])
        logger.debug(form['text_area'])
        logger.debug(form.cleaned_data)
    page = 'sb-admin/pages/mypages/%s.html' % page_name
    logger.debug(page)
    form = TestForm()


    return render(request, page, {'form': form})

def load_articles_form_page(request, page_name):
    logger.debug('go to load articles form')
    if request.method =='POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            logger.debug(form)
            save_it = form.save(commit=False)
            save_it.save()

            return HttpResponseRedirect(reverse('sb_admin:index'))

    else:
        form = ArticleForm()



    page = 'sb-admin/pages/mypages/%s.html' % page_name
    return render(request, page, {'form': form})


# Create Author
class ArticleCreate(CreateView):
    # model = Article
    form_class = ArticleForm
    # fields = ['pub_date', 'headline', 'content', 'reporter']
    template_name = 'sb-admin/pages/mypages/article_create_form.html'


    def get_success_url(self):
        return reverse('sb_admin:index')



# View Index List
class ArticleListView(generic.ListView):
    template_name = 'sb-admin/pages/mypages/article_list.html'
    context_object_name = 'article_list'

    def get_queryset(self):
        logger.debug(len(Article.objects.all()))
        return Article.objects.all()

# Detail Author
class ArticleDetailView(generic.DetailView):
    model = Article
    context_object_name = 'form'
    template_name = 'sb-admin/pages/mypages/article_detail_form.html'



# Update Form
class ArticleUpdate(UpdateView):
    model = Article
    fields = ['pub_date', 'headline', 'content', 'reporter']
    template_name = 'sb-admin/pages/mypages/article_update_form.html'

    def get_success_url(self):
        return reverse('sb_admin:index')


# Delete Author
class ArticleDelete(DeleteView):
    model = Article
    #success_url = reverse('generic_editing_view:index', args=[])
    context_object_name = 'article'
    template_name = 'sb-admin/pages/mypages/article_delete_form.html'

    def get_success_url(self):
        return reverse('sb_admin:index')

