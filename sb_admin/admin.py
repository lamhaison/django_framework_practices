from django.contrib import admin
from sb_admin.models import Options, Reporter, Article
# Register your models here.
admin.site.register(Options)
admin.site.register(Reporter)
admin.site.register(Article)
