from django.conf.urls import patterns, url
from sb_admin import views

urlpatterns = patterns('',
                       url(r'^$', views.load_sb_dash_board, name='sb_admin'),
                       url(r'^pages/(?P<page_name>[\w\-]+)/$', views.load_page, name='load_page'),
                       url(r'^mypages/(?P<page_name>[\w\-]+)/$', views.load_my_page, name='load_my_page'),
                       url(r'^index/$', views.ArticleListView.as_view(), name='index'),
                       url(r'^create/$', views.ArticleCreate.as_view(), name='article_create'),

                       url(r'^detail/(?P<pk>\d+)/$', views.ArticleDetailView.as_view(), name='article_detail'),
                       url(r'^update/(?P<pk>\d+)/$', views.ArticleUpdate.as_view(), name='article_update'),
                       url(r'^delete/(?P<pk>\d+)/$', views.ArticleDelete.as_view(), name='article_delete'),


                       )