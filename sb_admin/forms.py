__author__ = 'root'

from django import forms
from django.forms import widgets, ModelForm
from sb_admin.models import Options, Article, Reporter

FAVORITE_COLORS_CHOICES = (
    ('blue', 'Blue'),
    ('green', 'Green'),
    ('black', 'Black'),
)

CHOICES = (('1', 'First',), ('2', 'Second',))


class TestForm(forms.Form):
    char_field = forms.CharField(max_length=100,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control', 'placeholder': 'Enter here', 'type': 'text'}))

    text_area = forms.CharField(max_length=100,
                                widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter here'}))

    url = forms.URLField(widget=forms.URLInput(attrs={'class': 'form-control', 'placeholder': 'Enter your url'}))

    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))

    bool_field = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class': 'checkbox'}), required=True,
                                    label='Active')

    # Choice input data
    choice = forms.ChoiceField(widget=forms.RadioSelect(), choices=CHOICES)

    # Multiple check input
    multi_checkbox = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple(),
                                               choices=FAVORITE_COLORS_CHOICES)



    # Select list from statis list
    select_from_static = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=CHOICES)

    # Select options from database
    select_from_db = forms.ChoiceField(label='select  options fr db', choices=(),
                                       widget=forms.Select(attrs={'class': 'form-control'}))
    # Multiple select from database
    multi_select = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'class': 'form-control'}),
                                             choices=FAVORITE_COLORS_CHOICES)

    multi_select_from_db = forms.ModelMultipleChoiceField(queryset=Options.objects.all(),
                                                          widget=forms.SelectMultiple(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        super(TestForm, self).__init__(*args, **kwargs)
        choices = [(pt.id, unicode(pt.name)) for pt in Options.objects.all()]
        # choices.extend(EXTRA_CHOICES)
        self.fields['select_from_db'].choices = choices
        self.fields['multi_checkbox'].choices = choices
        self.fields['choice'].choices = choices
        # Load option list from database
        # self.fields['multi_select'].choices = choices



class ArticleForm(ModelForm):
    # reporter = forms.ModelChoiceField(queryset=Reporter.objects.all())
    content = forms.CharField(max_length=100, widget=forms.Textarea(attrs={'resize': 'none'}), label='Content')

    class Meta:
        model = Article
        fields = ['pub_date', 'headline', 'content', 'reporter']



