from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from datetime import datetime


# Create your views here.
'''
@require_http_methods(['GET'])
def get_page_home(request):
    return render(request, 'basic_view/home_page.html')
'''


def handler_404(request):
    return render(request, 'basic_view/404_page.html', status=404)

# Home page of directory /basic_view
# view root page same as base page in django admin template
def get_template_page(request):
    return render(request, 'basic_view/template.html')


# Inheritance template page, replace titile and content of base page
def get_current_date_time_page(request):
    return render(request, 'basic_view/current_datetime.html', {'current_date': datetime.now()})

#
def get_include_page(request):
    # return render(request, 'basic_view/base.html', {'header_part': 'basic_view/includes/header.html',
    #                                                         'footer_part': 'basic_view/includes/footer.html'})

    return render(request, 'basic_view/block_template.html')

