__author__ = 'root'
from django.conf.urls import patterns, url
from basic_view import views


urlpatterns = patterns('',
                       url(r'^$', views.get_template_page, name='template_page'),
                       url(r'^current_time/$', views.get_current_date_time_page, name='current_time_page'),
                       url(r'^include_page/$', views.get_include_page, name='include_page'),
)

