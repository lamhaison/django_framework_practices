__author__ = 'root'


class DbRouter(object):
    """
    A router to control all database operations on models in the
    auth application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models go to auth_db.
        """
        return model._meta.app_label

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to auth_db.
        """
        return model._meta.app_label

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth app is involved.
        """
        if obj1._meta.app_label == 'auth' or \
           obj2._meta.app_label == 'auth':
           return True
        return None


    def allow_syncdb(self, db, model):
        """
        Make sure the auth app only appears in the 'auth_db'
        database.
        """
        if db == 'tenantdb':
            return model._meta.app_label == 'tenantdb'
        elif model._meta.app_label == 'backupdb':
            return model._meta.app_label == 'backupdb'

        return None