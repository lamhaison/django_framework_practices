__author__ = 'root'
from django import forms
import logging
logger = logging.getLogger(__name__)


class ContactForm(forms.Form):
    name = forms.CharField(label='Author Name', max_length=100)
    message = forms.CharField(widget=forms.Textarea)

    def send_mail(self):
        # logger.debug('%s %s ' % (self.message, self.name))
        pass


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)


class ContactFormEx(forms.Form):
    subject = forms.CharField(max_length=100, help_text='Max 100 characters')
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField(error_messages={'required': 'Please enter sender email', 'invalid': 'invalid email'})
    cc_myself = forms.BooleanField(required=False)



