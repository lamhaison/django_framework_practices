__author__ = 'root'

from django.shortcuts import render
from generic_editing_view.forms import NameForm, ContactFormEx
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect(reverse('generic_editing_view:thanks'))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'form/name.html', {'form': form})

# controller for contact form
def get_contact(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ContactFormEx(request.POST)
        # check whether it's valid:

        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect(reverse('generic_editing_view:thanks'))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ContactFormEx(auto_id=False)

    return render(request, 'generic_editing_view/contact.html', {'form': form})


