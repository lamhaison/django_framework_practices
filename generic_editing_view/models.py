from django.db import models
from django.core.urlresolvers import reverse


# Create your models here.
class Author(models.Model):
    name = models.CharField(max_length=200)

    def get_absolute_url(self):
        return reverse('generic_editing_view:author_detail', kwargs={'pk': self.pk})



