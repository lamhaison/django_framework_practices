from django.conf.urls import patterns, url
from generic_editing_view import views
from generic_editing_view.controller import user

urlpatterns = patterns('',
                       # Link to dasbhboard, include all practices
                       
                       url(r'^index/$', views.IndexAuthorView.as_view(), name='index'),
                       # url(r'^contact/$', views.ContactView.as_view(), name='contact'),
                       url(r'^contact/$', user.get_contact, name='contact'),
                       url(r'^thanks/$', views.load_thank_page, name='thanks'),
                       url(r'^create/$', views.AuthorCreate.as_view(), name='author_create'),
                       url(r'^detail/(?P<pk>\d+)/$', views.AuthorDetailView.as_view(), name='author_detail'),
                       url(r'^update/(?P<pk>\d+)/$', views.AuthorUpdate.as_view(), name='author_update'),
                       url(r'^delete/(?P<pk>\d+)/$', views.AuthorDelete.as_view(), name='author_delete'),

                       url(r'^name/$', user.get_name, name='name'),


)