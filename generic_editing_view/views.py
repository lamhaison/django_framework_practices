from django.shortcuts import render
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.views import generic
from generic_editing_view.forms import ContactForm
from django.core.urlresolvers import reverse
from generic_editing_view.models import Author


def load_dash_board(request):
    return render(request, 'basic_view/base.html')


# Create your views here.
class ContactView(FormView):
    template_name = 'generic_editing_view/contact.html'
    form_class = ContactForm


    def get_success_url(self):
        return reverse('generic_editing_view:thanks')

    def form_valid(self, form):
        form.send_mail()
        return super(ContactView, self).form_valid(form)


def load_thank_page(request):
    return render(request, 'generic_editing_view/thank_page.html')

# Create Author
class AuthorCreate(CreateView):
    model = Author
    fields = ['name']
    template_name = 'generic_editing_view/author_form.html'


    def get_success_url(self):
        return reverse('generic_editing_view:index')


# Detail Author
class AuthorDetailView(generic.DetailView):
    model = Author
    context_object_name = 'author'
    template_name = 'generic_editing_view/author_detail_page.html'


# Update Form
class AuthorUpdate(UpdateView):
    model = Author
    fields = ['name']
    template_name = 'generic_editing_view/author_update_form.html'

    def get_success_url(self):
        return reverse('generic_editing_view:index')


# View Index List
class IndexAuthorView(generic.ListView):
    template_name = 'generic_editing_view/view_list.html'
    context_object_name = 'author_list'

    def get_queryset(self):
        return Author.objects.all()


# Delete Author
class AuthorDelete(DeleteView):
    model = Author
    #success_url = reverse('generic_editing_view:index', args=[])
    context_object_name = 'author'
    template_name = 'generic_editing_view/author_delete.html'

    def get_success_url(self):
        return reverse('generic_editing_view:index')

