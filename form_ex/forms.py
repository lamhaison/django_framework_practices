__author__ = 'root'

from django import forms
from sb_admin.models import Options
from django.forms.extras.widgets import SelectDateWidget
BIRTH_YEAR_CHOICES = ('1980', '1981', '1982')
FAVORITE_COLORS_CHOICES = (
    ('blue', 'Blue'),
    ('green', 'Green'),
    ('black', 'Black'),
)

CHOICES = (('1', 'First',), ('2', 'Second',))

class CommentForm(forms.Form):
    char_field = forms.CharField(max_length=100,
                                 widget=forms.TextInput())

    text_area = forms.CharField(max_length=100,
                                 widget=forms.Textarea())

    url = forms.URLField(widget=forms.URLInput())

    email = forms.EmailField(widget=forms.EmailInput())

    bool_field = forms.BooleanField(widget=forms.CheckboxInput(), required=True, label='Active')

    # Choice input data
    choice = forms.ChoiceField(widget=forms.RadioSelect(), choices=CHOICES)

    # Multiple check input
    multi_checkbox = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple(), choices=FAVORITE_COLORS_CHOICES)



    # Select list from statis list
    select_from_static = forms.ChoiceField(widget=forms.Select(), choices=CHOICES)

    # Select options from database
    select_from_db = forms.ChoiceField(label='select  options fr db', choices=(),
                                       widget=forms.Select())
    # Multiple select from database
    multi_select = forms.MultipleChoiceField(widget=forms.SelectMultiple(),
                                           choices=FAVORITE_COLORS_CHOICES)


    multi_select_from_db = forms.ModelMultipleChoiceField(queryset=Options.objects.all(),
                                                          widget=forms.SelectMultiple())




class LoginForm(forms.Form):
    username = forms.CharField(max_length=100, label='UserName', label_suffix=': ',
                               widget=forms.TextInput())

    password = forms.CharField(max_length=100, label='Password', label_suffix=': ',
                               widget=forms.PasswordInput())

    comment = forms.CharField(widget=forms.Textarea())
    multi_choice = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple(),
                                             choices=FAVORITE_COLORS_CHOICES, label='Multi Choise Demo')



    checkbox = forms.CheckboxInput()


