from django.conf.urls import patterns, url
from generic_editing_view import views
from form_ex import views

urlpatterns = patterns('',
                       url(r'(?P<page_name>\w+)/$',views.get_boot_strap_page, name='boot_strap'),
                       url(r'^$', views.load_dash_board),



)