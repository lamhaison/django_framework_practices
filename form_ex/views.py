from django.shortcuts import render, HttpResponseRedirect

from form_ex.forms import LoginForm, CommentForm

# Create your views here.

def get_boot_strap_page(request, page_name):
    page = 'form/bootstrap/%s.html' % page_name
    print page

    # Load Login page
    if page_name == 'login':
        return load_login_page(request)

    if page_name == 'bootstrapform':
        return load_bootstrap_form(request)

    return render(request, page)

def load_dash_board(request):
    return render(request, 'basic_view/base.html')


def load_login_page(request):
    form = LoginForm(auto_id=False)
    return render(request, 'form/bootstrap/login.html', {'form': form})


def load_bootstrap_form(request):
    form = CommentForm()
    return render(request, 'form/bootstrap/bootstrapform.html', {'form': form})

