from django.db import models

# Create your models here.


class Tenant(models.Model):
    tenant_id = models.CharField(max_length=64, unique=True)
    active = models.BooleanField(default=True)

    class Meta:
        db_table = 'tenants'
        app_label = 'tenantdb'
