"""
Django settings for django_app_practies project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!c*63-edthpxjsqhm-_hj8pd^jmqjweimfau#mv26-1-e+%det'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'basic_site',
    'viewgeneration',
    'basic_test_app',
    'basic_url',
    'basic_view',
    'generic_editing_view',
    'effective_django_app',
    'contrib_ex',
    'modelchapter',
    'form_ex',
    'sb_admin',
    'bootstrapform'

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

)

ROOT_URLCONF = 'django_app_practies.urls'

WSGI_APPLICATION = 'django_app_practies.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases


'''
DATABASES = {

    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'nova',
        'HOST': '10.200.0.4',
        'PORT': '3306',
        'USER': 'sonlh13',
        'PASSWORD': 'rad@1203',
    },

}
'''

DATABASES = {

    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'djangodb',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'USER': 'rad',
        'PASSWORD': 'rad@1203',
    },

}


'''
    'backupdb': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'backupdb',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'USER': 'backup',
        'PASSWORD': 'rad@1203',
    },

    'tenantdb': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tenantdb',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'USER': 'backup',
        'PASSWORD': 'rad@1203',
    }
'''

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

TIME_ZONE = 'Asia/Saigon'


USE_I18N = True

USE_L10N = True

USE_TZ = False

# Modify template place
# TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'effective_django_app/templates')]


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
# STATIC_ROOT = '/usr/share/django_framework_practices/static'

# Log configuration
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/django_practices.log',
            'formatter': 'verbose'
        },

        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
            'formatter': 'verbose'
        },
    },
    'loggers': {
        # 'django': {
        #    'handlers': ['file'],
        #    'propagate': True,
        #    'level': 'DEBUG',
        #},

        'basic_site': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
        },
        'viewgeneration': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
        },
        'basic_url': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
        },

        'generic_editing_view': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
        },

        'async_app': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
        },

        'contrib_ex': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
        },
        'sb_admin': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
        },


    }
}


# DATABASE_ROUTERS = ['utils.router.DbRouter']



# Rabbit mq config
import djcelery
djcelery.setup_loader()
BROKER_URL = "amqp://guest:guest@localhost:5672//"

CELERY_IMPORTS = ('async_app.tasks', )


# Email backuend
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.fpt.com.vn'
EMAIL_HOST_PASSWORD = '18032012je'
EMAIL_HOST_USER = 'sonlh13'
EMAIL_PORT = 587
EMAIL_SUBJECT_PREFIX = '[Service Monitor]'
DEFAULT_FROM_EMAIL = 'sonlh13@fpt.com.vn'
SERVER_EMAIL = 'sonlh13@fpt.com.vn'

ADMINS = (('sonlh13', 'sonlh13@fpt.com.vn'),)


ADMIN_LOGIN = 'admin'
ADMIN_PASSWORD = 'pbkdf2_sha256$12000$b2aCFfYjZ7IC$sYDL/OZa1OMUm4KNTs72yLbqyIANOUekx/6SgThw4hs='


AUTHENTICATION_BACKENDS = ('contrib_ex.authentication.SettingsBackend',)


import os.path
# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use  slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    # ('static', os.path.join(BASE_DIR, 'static')),
    os.path.join(BASE_DIR, 'static'),
)

# Additional locations of template files
TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates'), ]







