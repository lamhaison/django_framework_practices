from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'django_app_practies.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^dashboard/', include('form_ex.urls')),
                       url(r'^sb_admin/', include('sb_admin.urls', namespace='sb_admin')),
                       url(r'^$', include('sb_admin.urls', namespace='sb_admin')),

                       url(r'^bootstrap/', include('form_ex.urls', namespace='bootstrap')),
                       url(r'^polls/', include('basic_site.urls', namespace="polls")),
                       url(r'^generic_view/', include('viewgeneration.urls', namespace="generic_view")),
                       url(r'^url/', include('basic_url.urls', namespace="basic_url")),
                       url(r'^basic_view/', include('basic_view.urls', namespace='basic_view')),
                       url(r'^form/', include('generic_editing_view.urls',
                                                              namespace='generic_editing_view')),

                       url(r'^async/', include('async_app.urls',
                                               namespace='async_view')),

                       url(r'effective_django/', include('effective_django_app.urls',
                                                         namespace='effective_django')),
                       )

# Overwrite handler 404 page
handler404 = 'basic_view.views.handler_404'