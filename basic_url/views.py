from django.shortcuts import render

import logging
logger = logging.getLogger(__name__)


# Create your views here.
def page(request, num='1'):
    return render(request, 'basic_url/page.html', {'num': num})





