__author__ = 'root'
import views

# URLconf
from django.conf.urls import patterns, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^blog/$', views.page),
    url(r'^blog/page(?P<num>\d+)/$', views.page),

)




