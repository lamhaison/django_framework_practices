from django.db import models
from jsonfield import JSONField

# Create your models here.
class RollbackRestore(models.Model):
    vm_id = models.CharField(max_length=36)
    backup_id = models.CharField(max_length=100, primary_key=True)
    backup_data = JSONField()

    class Meta:
        db_table = 'rollback_restore_info'
        unique_together = ('vm_id', 'backup_id')

    #def get_rollback_name(self):
    #    return backup.get_backup_name_for_restore_backup(vm_id=self.vm_id, backup_name=self.backup_id,
    #                                          backup_time=self.backup_data['time'])


class FreeBackup(models.Model):
    vm_id = models.CharField(max_length=200)
    name = models.CharField(max_length=200, primary_key=True)
    backup_time = models.DateTimeField()

    class Meta:
        db_table = 'free_backup_info'
        unique_together = ('vm_id',)
        app_label = 'backupdb'