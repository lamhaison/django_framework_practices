from django.shortcuts import render
from viewgeneration.models import Reporter, Article
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.utils import timezone
import logging
logger = logging.getLogger(__name__)


# Create your views here.
class ArticleDetailView(DetailView):

    model = Article
    template_name = 'viewgeneration/article_detail.html'
    context_object_name = 'article'

    # Specified filed to query get object which will be view detail
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        logger.debug(self.get_slug_field())
        return context

class ArticleListView(ListView):

    model = Article
    template_name = 'viewgeneration/article_list.html'
    context_object_name = 'article_list'

    # Get list data and add now filed to response
    def get_context_data(self, **kwargs):
        context = super(ArticleListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context
