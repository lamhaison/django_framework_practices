__author__ = 'root'
from django.conf.urls import patterns, url
from viewgeneration import views


urlpatterns = patterns('',
                       url(r'^(?P<slug>[-_\w]+)/$', views.ArticleDetailView.as_view(), name='article-detail'),
                       url(r'^$', views.ArticleListView.as_view(), name='article-list'),
)