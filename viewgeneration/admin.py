from django.contrib import admin
from viewgeneration.models import Article, Reporter

# Register your models here.

admin.site.register(Article)
admin.site.register(Reporter)

