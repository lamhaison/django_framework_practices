from django.shortcuts import render
from django.views.generic import View, ListView
from django.views.generic import CreateView
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.views.generic import UpdateView

from effective_django_app.models import Contact

class ListContactView(ListView):
    model = Contact
    template_name = 'contact_list.html'

class CreateContactView(CreateView):
    model = Contact
    template_name = 'edit_contact.html'
    fields = ['first_name', 'last_name', 'email']

    def get_success_url(self):
        return reverse('effective_django:contacts-list')



class UpdateContactView(UpdateView):

    model = Contact
    template_name = 'edit_contact.html'

    def get_success_url(self):
        return reverse('effective_django:contacts-list')
