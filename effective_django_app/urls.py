__author__ = 'lhs'
from django.conf.urls import url, include, patterns
from effective_django_app import views

urlpatterns = patterns('',
    url(r'^contacts/$', views.ListContactView.as_view(), name='contacts-list'),
    url(r'^new/$', views.CreateContactView.as_view(), name='contacts-new'),
    url(r'^edit/(?P<pk>\d+)/$', views.UpdateContactView.as_view(), name='contacts-edit'),



)


