from django.test import TestCase
from django.test.client import Client
from django.test.client import RequestFactory
from effective_django_app.views import ListContactView
from django.core.urlresolvers import reverse

# Create your tests here.
from effective_django_app.models import Contact
class ContactTests(TestCase):
    """Contact model tests."""

    def test_str(self):

        contact = Contact(first_name='John', last_name='Smith')

        self.assertEquals(
            str(contact),
            'John Smith',
        )




class ContactListViewTestCase(TestCase):
    def test_contacts_in_the_context(self):
        client = Client()
        url_contacts = reverse('effective_django:contacts-list')
        response = client.get(url_contacts)
        self.assertEquals(list(response.context['object_list']), [])

        Contact.objects.create(first_name='foo', last_name='bar')
        response = client.get(url_contacts)
        self.assertEquals(response.context['object_list'].count(), 1)


    def test_contacts_in_the_context_request_factory(self):
        factory = RequestFactory()
        url_contacts = reverse('effective_django:contacts-list')
        request = factory.get(url_contacts)

        response = ListContactView.as_view()(request)

        self.assertEquals(list(response.context_data['object_list']), [])

        Contact.objects.create(first_name='foo', last_name='bar')
        response = ListContactView.as_view()(request)
        self.assertEquals(response.context_data['object_list'].count(), 1)

